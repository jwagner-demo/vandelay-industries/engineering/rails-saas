## Story

1. **As a** [user concerned by the story]
1. **I want** [goal of the story]
1. **so that** [reason for the story]


## Description

[Add more information here:]


## Acceptance criteria

- [ ] This is something that can be verified to show that this user story is satisfied.

## Sprint Ready Checklist 

1. - [ ] Acceptance criteria defined 
2. - [ ] Team understands acceptance criteria 
3. - [ ] Team has defined solution / steps to satisfy acceptance criteria 
4. - [ ] Acceptance criteria is verifiable / testable 
5. - [ ] External / 3rd Party dependencies identified 

## Resources:

* Mockups: [Here goes a URL to or the name of the mockup(s) in inVision];
* Testing URL: [Here goes a URL to the testing branch or IP];
* Staging URL: [Here goes a URL to the feature on staging];


## Notes

[Some complementary notes if necessary:]

/label "User Story"
/label "Status::To Do"

